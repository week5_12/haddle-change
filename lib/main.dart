import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Retrieve Text Input',
      home: MyCustomeForm(),
    );
  }
}

class MyCustomeForm extends StatefulWidget {
  MyCustomeForm({Key? key}) : super(key: key);

  @override
  _MyCustomeFormState createState() => _MyCustomeFormState();
}

class _MyCustomeFormState extends State<MyCustomeForm> {
  final myController = TextEditingController();
  @override
  void initState() {
    myController.addListener(_printLastestValue);
    super.initState();
  }
@override
  void dispose() {
    myController.dispose();
    super.dispose();
  }
  void _printLastestValue(){
    print('Second text field : ${myController.text}');
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Retrieve Text Input'),
      ),
      body: Padding(
        padding: EdgeInsets.all(16.0),
        child: Column(
          children: [
            TextField(onChanged: (text) {
              print('First text field: $text');
            }
            ),
            TextField(
              controller: myController,
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showDialog(context: context, builder: (context){
            return AlertDialog(
              content: Text(myController.text),
            );
          });
        },
        tooltip: 'Show me the value',
        child: Icon(Icons.text_fields),
      ),
    );
  }
}
